import { createContext, useState, useCallback } from "react";

const BooksContext = createContext();

function Provider ({children}) {

    const [books, setBooks] = useState([]);
    const url = "http://localhost:3001/books";

    const fetchBooks = useCallback ( async () => {
        const response = await fetch(url, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            headers: {
              'Content-Type': 'application/json'
 
            },
          });
          let result = await response.json();
          
        setBooks(result);
    }, []);

    const updateBookById = async (id, newtitle) => {
        const url = `http://localhost:3001/books/${id}`;
        const data = { title: newtitle };
        const response = await fetch(url, {
            method: 'PUT', // *GET, POST, PUT, DELETE, etc.
            headers: {
              'Content-Type': 'application/json'
 
            },
            body: JSON.stringify(data),
          });
          let result = await response.json();
        

        const updatedBooks = books.map ((book) => {
            if (book.id === id) {
                return {...book, ...result}
            }
            return book;
        });
        setBooks(updatedBooks);
    };
    
    const deleteBookById = async (id) => {
        const url = `http://localhost:3001/books/${id}`;
        
        const response = await fetch(url, {
            method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
            headers: {
              'Content-Type': 'application/json'
 
            },
          });
          let result = await response.json();

        const updatedBooks = books.filter ((book) => {
            return book.id !== id;
        });
        setBooks(updatedBooks);
    };
    
    const createBook = async (title) => {
        
        const data = { title };

        const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            headers: {
              'Content-Type': 'application/json'
 
            },
            body: JSON.stringify(data),
          });
          let result = await response.json();
 
        const updatedBooks = [
            ...books,
            { id: result.id, title: result.title}
        ];
        setBooks(updatedBooks);
    };
    const valueToShare = {
        books,
        createBook,
        deleteBookById,
        updateBookById,
        fetchBooks,
    }
    return <BooksContext.Provider value = {valueToShare}>
        {children}
        </BooksContext.Provider>
}
export {Provider};
export default BooksContext;